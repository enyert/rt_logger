var express = require('express');
var router = express.Router();

//database
var mongo = require('mongoskin');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'RT_Logger API' });
});

module.exports = router;
