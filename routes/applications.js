//Dependencies
var express = require('express');
var crypto = require('crypto');
var router = express.Router();


/**
 * Basic CRUD Operations
 */

/* GET applications listing. */
router.get('/', function(req, res) {
    var db = req.db;
    db.collection('applications').find().toArray(function (err, items) {
        res.status(200).json(items);
    });
});

/* GET Get application by Id*/
router.get('/:id', function(req, res) {
    var db = req.db;
    db.collection('applications').findById(req.params.id, function (err, items) {
        res.status(200).json(items);
    });
});

/* POST Add an application */
router.post('/', function(req, res){
    var db = req.db;
    var application = req.body;
    application.created = new Date();
    db.collection('applications').insert(application, function(err, result){
        if(err) throw err;
        if(result){
            res.status(201).json(application)
        }
    })
});

// DELETE Delete application by id

router.delete('/:id', function(req, res){
    var db = req.db;
    db.collection('applications').removeById(req.params.id, function(err, result){
        if(err)
            throw err;
        else {
            res.status(204).send({'Object Deleted': req.params.id});
        }
    });
});


// PUT Update an application by id
router.put('/:id', function(req, res){
    var db = req.db;
    var criteria = req.body;
    db.collection('applications').updateById(req.params.id,{$set: criteria}, function(err, result){
        if(err)
            throw err;
        else
            res.status(200).send({msg: 'success'});
    });
});

module.exports = router;
