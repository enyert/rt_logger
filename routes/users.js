//Dependencies
var express = require('express');
var crypto = require('crypto');
var router = express.Router();


/**
 * Basic CRUD Operations
 */

/* GET users listing. */
router.get('/', function(req, res) {
  var db = req.db;
  db.collection('users').find().toArray(function (err, items) {
    res.status(200).json(items);
  });
});

/* GET Get user by Id*/
router.get('/:id', function(req, res) {
  var db = req.db;
  db.collection('users').findById(req.params.id, function (err, items) {
    res.status(200).json(items);
  });
});

/* POST Add an user */
router.post('/', function(req, res){
  var db = req.db;
  var usuario = req.body;
  var hash = crypto.createHash('md5').update(usuario.password).digest('hex');
  usuario.passwordMD5 = hash;
  usuario.created = new Date();
  delete usuario.password;
  db.collection('users').insert(usuario, function(err, result){
    if(err) throw err;
    if(result){
      console.log('User added: ' + usuario.username);
      res.status(201).json(usuario)
    }
  })
});

// DELETE Delete user by id

router.delete('/:id', function(req, res){
  var db = req.db;
  db.collection('users').removeById(req.params.id, function(err, result){
    if(err)
      throw err;
    else {
      res.status(204).send({'Object Deleted': req.params.id});
    }
  });
});


// PUT Update a user by id
router.put('/:id', function(req, res){
  var db = req.db;
  var criteria = req.body;
  db.collection('users').updateById(req.params.id,{$set: criteria}, function(err, result){
    if(err)
      throw err;
    else
      res.status(200).send({msg: 'success'});
  });
});

// function generateToken(){
//   return crypto.randomBytes(64).toString('hex');
// }
//
// for(var i = 0; i < 3; i++){
//   console.log(generateToken());
// }

module.exports = router;
