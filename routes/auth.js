//Dependencies
var express = require('express');
var crypto = require('crypto');
var router = express.Router();

router.post('/', function(req, res){
  var db = req.db;
  var usuario = req.user;
  db.collection('users').findOne({username: usuario.username, password: usuario.password}, function(err, user){
    if(err){
      res.json({
        type: false,
        data: "Error: " + err
      });
    }else{
      if(user){
        if(user.isActive){
          res.json({
            type: true,
            data: user,
            token: user.token
          });
        }else{
          res.json({
            type: false,
            data: "User is not active"
          });
        }
      }else{
        res.json({
          type: false,
          data: "Incorrect email/password"
        });
      }
    }
  });
});
