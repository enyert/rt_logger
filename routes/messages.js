//Dependencies
var express = require('express');
var crypto = require('crypto');
var router = express.Router();


/**
 * Basic CRUD Operations
 */

/* GET users listing. */
router.get('/', function(req, res) {
    var db = req.db;
    db.collection('messages').find().toArray(function (err, items) {
        res.status(200).json(items);
    });
});

/* GET Get user by Id*/
router.get('/:id', function(req, res) {
    var db = req.db;
    db.collection('messages').findById(req.params.id, function (err, items) {
        res.status(200).json(items);
    });
});

/* POST Add an user */
router.post('/', function(req, res){
    var db = req.db;
    var message = req.body;
    message.created = new Date();
    db.collection('messages').insert(message, function(err, result){
        if(err) throw err;
        if(result){
            res.status(201).json(message)
        }
    })
});

/* DELETE Delete user by id */
router.delete('/:id', function(req, res){
    var db = req.db;
    db.collection('messages').removeById(req.params.id, function(err, result){
        if(err)
            throw err;
        else {
            res.status(204).send({'Object Deleted': req.params.id});
        }
    });
});


// PUT Update a user by id
router.put('/:id', function(req, res){
    var db = req.db;
    var criteria = req.body;
    db.collection('messages').updateById(req.params.id,{$set: criteria}, function(err, result){
        if(err)
            throw err;
        else
            res.status(200).send({msg: 'success'});
    });
});

module.exports = router;